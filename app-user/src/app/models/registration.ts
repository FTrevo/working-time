export class Registration {
  start: string;
  end: string;

  constructor (start: string, end: string) {
      this.start = start;
      this.end = end;
  }
}
