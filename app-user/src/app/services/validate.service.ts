import { Injectable } from '@angular/core';
import * as Moment from 'moment';

@Injectable()
export class ValidateService {

  constructor() { }

  private regexValidation = /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/;
  private regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  validateLogin(loginUser){
    if(loginUser.userName && loginUser.password){
      return true;
    } else {
      return false;
    }
  }

  validateRegister(user){
    if(user.name && user.userName && user.email && user.password){
      return true;
    } else {
      return false;
    }
  }

  validateEmail(email){
    return this.regexEmail.test(email);
  }

  validateTimeRegistration(registration){
    return this.regexValidation.test(registration.start) && this.regexValidation.test(registration.end) && registration.start < registration.end;
  }

  validateTimeRegistrations(registrations){
    if(registrations){
      for (let registration of registrations) {
          if(!this.validateTimeRegistration(registration)){
            return false;
          }
      }
      return true;
    }
    return false;
  }

  validateBreaks(registrations){
    let previousEnd = '';
    for (let registration of registrations) {
        if(previousEnd){
          let previousEndTime = Moment(previousEnd, "HH:mm");
          let actualStartTime = Moment(registration.start, "HH:mm");

          if(!previousEndTime.isBefore(actualStartTime)){
            return false;
          }
        }
        previousEnd = registration.end;
    }
    return true;
  }

  isValidPeriod(start, end){
    if(start && end){
      return Moment(start).isSameOrBefore(end, 'day');
    }
    return true;
  }
}
