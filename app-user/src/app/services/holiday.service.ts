import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';

import { AuthGuard } from '../guards/auth.guard';

import 'rxjs/add/operator/map';

@Injectable()
export class HolidayService {

  private url = environment.baseUrl + 'holidays';

  constructor(
    private authGuard: AuthGuard,
    private http: Http
  ) { }

  getHolidays(filter){
    this.authGuard.canActivate();

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', localStorage.getItem('id_token'));

    let params: URLSearchParams = new URLSearchParams();
    params.set('startDate', filter.startDate);
    params.set('endDate', filter.endDate);

    let requestOptions = new RequestOptions({ headers: headers, search: params });

    return this.http.get(this.url + '/list', requestOptions ).map( response => response.json() );
  }

  getHolidayById(id){
    this.authGuard.canActivate();

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', localStorage.getItem('id_token'));

    return this.http.get(this.url + '/' + id, { headers: headers } ).map( response => response.json() );
  }

  saveOrUpdateHoliday(holiday){
    this.authGuard.canActivate();

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', localStorage.getItem('id_token'));

    if(holiday._id){
      return this.http.post(this.url + '/update', holiday, { headers: headers } ).map( response => response.json() );
    } else {
      return this.http.post(this.url +  '/save', holiday, { headers: headers } ).map( response => response.json() );
    }
  }

  removeHolidayById(id){
    this.authGuard.canActivate();

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', localStorage.getItem('id_token'));

    return this.http.delete(this.url + '/' + id, { headers: headers } ).map( response => response.json() );
  }
}
