import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';

import { AuthGuard } from '../guards/auth.guard';

import 'rxjs/add/operator/map';

@Injectable()
export class DailyReportService {

  private url = environment.baseUrl + 'daily-reports';

  constructor(
    private authGuard: AuthGuard,
    private http: Http
  ) { }

  getDailyReports(filter){
    this.authGuard.canActivate();

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', localStorage.getItem('id_token'));

    let params: URLSearchParams = new URLSearchParams();
    params.set('startDate', filter.startDate);
    params.set('endDate', filter.endDate);

    let requestOptions = new RequestOptions({ headers: headers, search: params });

    return this.http.get(this.url + '/list', requestOptions ).map( response => response.json() );
  }

  saveDailyReport(dailyReport){
    this.authGuard.canActivate();

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', localStorage.getItem('id_token'));

    return this.http.post(this.url +  '/save', dailyReport, { headers: headers } ).map( response => response.json() );
  }

  updateDailyReport(dailyReport){
    this.authGuard.canActivate();

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', localStorage.getItem('id_token'));

    return this.http.post(this.url + '/update', dailyReport, { headers: headers } ).map( response => response.json() );
  }

  getDailyReportById(id){
    this.authGuard.canActivate();

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', localStorage.getItem('id_token'));

    return this.http.get(this.url + '/' + id, { headers: headers } ).map( response => response.json() );
  }

  removeDailyReportById(id){
    this.authGuard.canActivate();

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', localStorage.getItem('id_token'));

    return this.http.delete(this.url + '/' + id, { headers: headers } ).map( response => response.json() );
  }
}
