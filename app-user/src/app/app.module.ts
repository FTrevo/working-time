import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ModalModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';

import { RouterModule, Routes } from '@angular/router'
import { FlashMessagesModule } from 'angular2-flash-messages';
import { MyDatePickerModule } from 'mydatepicker';
import { TextMaskModule } from 'angular2-text-mask';

import { AppComponent } from './app.component';
import { DailyRegistrationComponent } from './components/daily-registration/daily-registration.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HolidayComponent } from './components/holiday/holiday.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RegisterComponent } from './components/register/register.component';

import { AuthGuard } from './guards/auth.guard';

import { AuthService } from './services/auth.service';
import { DailyReportService } from './services/daily-report.service';
import { HolidayService } from './services/holiday.service';
import { ValidateService } from './services/validate.service';

const appRoutes: Routes = [

  {path: '', component: HomeComponent},
  {path: 'daily-registration/:id', component: DailyRegistrationComponent, canActivate: [AuthGuard]},
  {path: 'daily-registration', component: DailyRegistrationComponent, canActivate: [AuthGuard]},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'holiday', component: HolidayComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'register', component: RegisterComponent},

  {path: '**', redirectTo: '/'}

];

@NgModule({
  declarations: [
    AppComponent,
    DailyRegistrationComponent,
    DashboardComponent,
    HomeComponent,
    LoginComponent,
    NavbarComponent,
    ProfileComponent,
    RegisterComponent,
    HolidayComponent
  ],
  imports: [
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    BrowserModule,
    FlashMessagesModule,
    FormsModule,
    HttpModule,
    MyDatePickerModule,
    TextMaskModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthGuard,
    AuthService,
    DailyReportService,
    HolidayService,
    ValidateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
