import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import * as Moment from 'moment';

import { HolidayService } from '../../services/holiday.service';
import { ValidateService } from '../../services/validate.service';

@Component({
  selector: 'app-holiday',
  templateUrl: './holiday.component.html',
  styleUrls: ['./holiday.component.css']
})
export class HolidayComponent implements OnInit {

  startDate: any
  endDate: any
  holidays: [any]

  constructor(
    private holidayService: HolidayService,
    private flashMessage: FlashMessagesService,
    private validateService: ValidateService
  ) { }

  ngOnInit() {
    let momentYear = Moment().year();

    this.startDate = { date: { year: momentYear, month:1, day: 1 }, jsdate: Moment('01/01/'+momentYear, 'DD/MM/YYYY').toDate()};
    this.endDate = { date: { year: momentYear, month:12, day: 31 }, jsdate: Moment('31/12/'+momentYear, 'DD/MM/YYYY').toDate()};

    this.onClickFindHolidays();
  }

  onClickFindHolidays(){
    let filter : any = {};

    if(this.startDate){
      filter.startDate = this.startDate.jsdate;
    }
    if(this.endDate){
      filter.endDate = this.endDate.jsdate;
    }

    if(!this.validateService.isValidPeriod(filter.startDate, filter.endDate)){
      this.flashMessage.show('Período inválido.', {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      return false;
    }

    this.holidayService.getHolidays(filter).subscribe(data => {
      this.holidays = data.holidays;
    });
  }
}
