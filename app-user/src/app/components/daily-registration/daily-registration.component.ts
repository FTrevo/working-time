import { Component, OnInit } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { Router, ActivatedRoute } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import * as Moment from 'moment';

import { DailyReportService } from '../../services/daily-report.service';
import { ValidateService } from '../../services/validate.service';

import { Registration } from '../../models/registration';

@Component({
  selector: 'app-daily-registration',
  templateUrl: './daily-registration.component.html',
  styleUrls: ['./daily-registration.component.css']
})
export class DailyRegistrationComponent implements OnInit {

  timeMask: Array<string | RegExp>
  date: any
  registrations: [Registration]
  disabledDate: boolean = false
  private id: string
  private userId: string

  constructor(
    private dailyReportService: DailyReportService,
    private flashMessage: FlashMessagesService,
    private route: ActivatedRoute,
    private router: Router,
    private validateService: ValidateService
  ) { }

  ngOnInit() {
    this.date = '';
    this.registrations = [new Registration('', '')];

    this.route.params.subscribe(params => {
      this.id = params['id'];

      if(this.id){
        this.disabledDate = true;

        //Consulta o Registro Diário
        this.dailyReportService.getDailyReportById(this.id).subscribe(data => {
          if(data.success){
            let momentDate = Moment(data.dailyReport.date);

            this.date = { date: { year: momentDate.year(), month: momentDate.month() + 1, day: momentDate.date() }, jsdate: data.dailyReport.date};

            data.dailyReport.registrations.forEach((item, index) => {
              this.registrations[index] = new Registration(item.start, item.end);
            });

          } else {
            this.flashMessage.show(data.message, {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
          }
        });
      }
    });

    this.timeMask = [ /[0-2]/, /[0-9]/, ':', /[0-5]/, /[0-9]/];
  }

  onClickAddRegistration(){
    this.registrations.push(new Registration('', ''));
  }

  onClickRemoveRegistration(index){
    this.registrations.splice(index, 1);
  }

  onDailyRegistrationSubmit(){
    let validForm = true;

    //Campos obrigatórios
    if(!this.date || !this.registrations){
      this.flashMessage.show('Preencha todos os campos.', {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      validForm = false;
    }

    //Valida os horários informados
    if(!this.validateService.validateTimeRegistrations(this.registrations)){
      this.flashMessage.show('Registro inválido.', {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      validForm = false;

    } else if(!this.validateService.validateBreaks(this.registrations)){
      //Valida os intervalos informados
      this.flashMessage.show('Intervalo inválido.', {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      validForm = false;
    }

    if(validForm){
      let dailyReport = {
        _id: this.id,
        date: this.date.jsdate,
        registrations: this.registrations,
        dailyResult: this.getDailyTotal()
      }

      //Salva ou atualiza o Registro Diário
      if(this.id){
        this.dailyReportService.updateDailyReport(dailyReport).subscribe(data => {
          if(data.success){
            this.flashMessage.show(data.message, {cssClass: 'alert-success', timeout: 3000 }); //Timeout em ms.
            this.router.navigate(['/dashboard']);
          } else {
            this.flashMessage.show(data.message, {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
          }
        });
      } else {
        this.dailyReportService.saveDailyReport(dailyReport).subscribe(data => {
          if(data.success){
            this.flashMessage.show(data.message, {cssClass: 'alert-success', timeout: 3000 }); //Timeout em ms.
            this.ngOnInit();
          } else {
            this.flashMessage.show(data.message, {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
          }
        });
      }
    }
  }

  getDailyTotal(){
    let value = 0;

    for (let registration of this.registrations) {
      let startTime = Moment(registration.start, "HH:mm");
      let endTime = Moment(registration.end, "HH:mm");
      let duration = endTime.diff(startTime, 'minutes');

      if(duration > 0){
        value = value + duration;
      }
    }

    let pad = '00';
    let hour = '' + Math.floor(value/60);
    let minute = ''+ value % 60;

    return (pad.substring(0, pad.length - hour.length) + hour) + ':' + (pad.substring(0, pad.length - minute.length) + minute);
  }
}
