import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../services/auth.service';
import { ValidateService } from '../../services/validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userName: String;
  password: String;

  constructor(
    private authService: AuthService,
    private validateService: ValidateService,
    private flashMessage: FlashMessagesService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  onLoginSubmit(){
    const user ={
      userName: this.userName,
      password: this.password
    }

    if(!this.validateService.validateLogin(user)){
      this.flashMessage.show('Preencha todos os campos.', {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      return false;
    }

    this.authService.authenticateUser(user).subscribe(data => {
      if(data.success){
        this.authService.storeUserData(data.token);
        this.router.navigate(['/dashboard']);
      } else {
        this.flashMessage.show(data.message, {cssClass: 'alert-danger', timeout: 5000 }); //Timeout em ms.
        this.router.navigate(['/login']);
      }
    });
  }
}
