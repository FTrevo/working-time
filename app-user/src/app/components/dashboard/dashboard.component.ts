import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import * as Moment from 'moment';

import { DailyReportService } from '../../services/daily-report.service';
import { ValidateService } from '../../services/validate.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  dailyReports: any;
  startDate: any;
  endDate: any;
  reportsTotalWorkedTime: string;

  constructor(
    private dailyReportService: DailyReportService,
    private flashMessage: FlashMessagesService,
    private router: Router,
    private validateService: ValidateService
  ) { }

  ngOnInit() {
    let momentYear = Moment().year();

    this.startDate = { date: { year: momentYear, month:1, day: 1 }, jsdate: Moment('01/01/'+momentYear, 'DD/MM/YYYY').toDate()};
    this.endDate = { date: { year: momentYear, month:12, day: 31 }, jsdate: Moment('31/12/'+momentYear, 'DD/MM/YYYY').toDate()};

    this.onClickFindDailyReports();
  }

  onClickRemoveDailyReport(id){
    this.dailyReportService.removeDailyReportById(id).subscribe(data => {
      if(data.success){
        this.flashMessage.show(data.message, {cssClass: 'alert-success', timeout: 3000 }); //Timeout em ms.
        this.ngOnInit();
      } else {
        this.flashMessage.show(data.message, {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      }
    });
  }

  onClickFindDailyReports(){
    let filter : any = {};

    if(this.startDate){
      filter.startDate = this.startDate.jsdate;
    }
    if(this.endDate){
      filter.endDate = this.endDate.jsdate;
    }

    if(!this.validateService.isValidPeriod(filter.startDate, filter.endDate)){
      this.flashMessage.show('Período inválido.', {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      return false;
    }

    this.dailyReportService.getDailyReports(filter).subscribe( data => {
      if(data.success){
        this.dailyReports = data.dailyReports;
        this.reportsTotalWorkedTime = this.getTotalWorkedHours(data.dailyReports);
      } else {
        this.flashMessage.show(data.message, {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
        this.reportsTotalWorkedTime = '-'
      }
    });
  }

  getTotalWorkedHours(dailyReports){
    let totalMinute = 0;

    for (let dailyReport of dailyReports) {
      let dailyTotal = Moment(dailyReport.dailyResult, 'HH:mm');
      totalMinute += dailyTotal.get('minute');
      totalMinute += (60 * dailyTotal.get('hour'));
    }

    let hour = '' + Math.floor(totalMinute/60);
    let minute = ''+ totalMinute % 60;

    let pad = '00';

    return (pad.substring(0, pad.length - hour.length) + hour) + ':' + (pad.substring(0, pad.length - minute.length) + minute);
  }
}
