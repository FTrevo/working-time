import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ModalModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';

import { RouterModule, Routes } from '@angular/router'
import { FlashMessagesModule } from 'angular2-flash-messages';
import { MyDatePickerModule } from 'mydatepicker';
import { TextMaskModule } from 'angular2-text-mask';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RegisterComponent } from './components/register/register.component';

import { AdminHolidayComponent } from './components/admin-holiday/admin-holiday.component';

import { AuthGuard } from './guards/auth.guard';

import { AuthService } from './services/auth.service';
import { HolidayService } from './services/holiday.service';
import { ValidateService } from './services/validate.service';

const appRoutes: Routes = [

  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},

  {path: 'admin-holiday', component: AdminHolidayComponent, canActivate: [AuthGuard]},

  {path: '**', redirectTo: '/'}

];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NavbarComponent,
    RegisterComponent,
    AdminHolidayComponent
  ],
  imports: [
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    BrowserModule,
    FlashMessagesModule,
    FormsModule,
    HttpModule,
    MyDatePickerModule,
    TextMaskModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthGuard,
    AuthService,
    HolidayService,
    ValidateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
