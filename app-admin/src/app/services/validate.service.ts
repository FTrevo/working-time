import { Injectable } from '@angular/core';
import * as Moment from 'moment';

@Injectable()
export class ValidateService {

  constructor() { }

  private regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  validateLogin(loginUser){
    if(loginUser.userName && loginUser.password){
      return true;
    } else {
      return false;
    }
  }

  validateRegister(user){
    if(user.name && user.userName && user.email && user.password){
      return true;
    } else {
      return false;
    }
  }

  validateEmail(email){
    return this.regexEmail.test(email);
  }

  isValidPeriod(start, end){
    if(start && end){
      return Moment(start).isSameOrBefore(end, 'day');
    }
    return true;
  }
}
