import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

import { Http, Headers } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  private url = environment.baseUrl + 'users';

  authToken: any;

  constructor(
    private http: Http,
    private router: Router
  ) { }

  registerUser(user){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.url + '/register', user, { headers: headers } ).map( response => response.json() );
  }

  authenticateUser(user){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.url + '/authenticate-admin', user, { headers: headers } ).map( response => response.json() );
  }

  loadToken(){
    this.authToken = localStorage.getItem('id_token');
  }

  storeUserData(token){
    localStorage.setItem('id_token', token);

    this.authToken = token;
  }

  loggedIn(){
    return tokenNotExpired('id_token');
  }

  logout(){
    localStorage.clear();
    this.authToken = null;
  }
}
