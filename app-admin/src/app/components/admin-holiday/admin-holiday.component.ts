import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FlashMessagesService } from 'angular2-flash-messages';
import * as Moment from 'moment';

import { HolidayService } from '../../services/holiday.service';
import { ValidateService } from '../../services/validate.service';

@Component({
  selector: 'app-admin-holiday',
  templateUrl: './admin-holiday.component.html',
  styleUrls: ['./admin-holiday.component.css']
})
export class AdminHolidayComponent implements OnInit {

  @ViewChild('manageHolidayModal') public manageHolidayModal:ModalDirective;

  startDate: any
  endDate: any
  holidays: [any]
  candidateHoliday: any
  modalTitle: any
  modalMessages: [any]
  disabledDate: boolean = false

  constructor(
    private holidayService: HolidayService,
    private flashMessage: FlashMessagesService,
    private validateService: ValidateService
  ) { }

  ngOnInit() {
    let momentYear = Moment().year();

    this.startDate = { date: { year: momentYear, month:1, day: 1 }, jsdate: Moment('01/01/'+momentYear, 'DD/MM/YYYY').toDate()};
    this.endDate = { date: { year: momentYear, month:12, day: 31 }, jsdate: Moment('31/12/'+momentYear, 'DD/MM/YYYY').toDate()};

    this.candidateHoliday = {};

    this.onClickFindHolidays();
  }

  onClickFindHolidays(){
    let filter : any = {};

    if(this.startDate){
      filter.startDate = this.startDate.jsdate;
    }
    if(this.endDate){
      filter.endDate = this.endDate.jsdate;
    }

    if(!this.validateService.isValidPeriod(filter.startDate, filter.endDate)){
      this.flashMessage.show('Período inválido.', {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      return false;
    }

    this.holidayService.getHolidays(filter).subscribe(data => {
      this.holidays = data.holidays;
    });
  }

  onClickAddHoliday(){
    this.candidateHoliday = {};

    let momentDate = Moment();
    this.candidateHoliday.date = { date: { year: momentDate.year(), month: momentDate.month() + 1, day: momentDate.date() }, jsdate: momentDate.toDate()};

    this.modalMessages = undefined;
    this.disabledDate = false;
    this.manageHolidayModal.show();
  }

  onClickUpdateHoliday(id){
    this.candidateHoliday = {};

    //Consulta o Registro Diário
    this.holidayService.getHolidayById(id).subscribe(data => {
      if(data.success){
        this.disabledDate = true;
        this.candidateHoliday = data.holiday;

        let momentDate = Moment(data.holiday.date);

        this.candidateHoliday.date = { date: { year: momentDate.year(), month: momentDate.month() + 1, day: momentDate.date() }, jsdate: data.holiday.date};
        this.manageHolidayModal.show();
      } else {
        this.flashMessage.show(data.message, {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      }
    });

  }

  onClickRemoveHoliday(id){
    this.holidayService.removeHolidayById(id).subscribe(data => {
      if(data.success){
        this.flashMessage.show(data.message, {cssClass: 'alert-success', timeout: 3000 }); //Timeout em ms.
        this.onClickFindHolidays();
      } else {
        this.flashMessage.show(data.message, {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      }
    });
  }

  onHolidaySubmit(){
    let validForm = true;

    //Campos obrigatórios
    if(!this.candidateHoliday.date || !this.candidateHoliday.accreditHours || !this.candidateHoliday.description){
      validForm = false;
      this.modalMessages = [{cssClass: 'alert-danger', text: 'Preencha todos os campos.'}];
    }

    if(this.candidateHoliday.accreditHours && this.candidateHoliday.accreditHours > 8){
      validForm = false;

      let alert = {cssClass: 'alert-danger', text: 'O campo \'Horas abonadas\' deve conter um valor entre 1 e 8 horas.'};
      if(this.modalMessages){
        this.modalMessages.push(alert);
      } else {
        this.modalMessages = [alert];
      }
    }

    if(validForm){
      //Salva ou atualiza o Feriado
      this.holidayService.saveOrUpdateHoliday(this.candidateHoliday).subscribe(data => {
        if(data.success){
          this.manageHolidayModal.hide();
          this.flashMessage.show(data.message, {cssClass: 'alert-success', timeout: 3000 }); //Timeout em ms.
          this.onClickFindHolidays();
        } else {
          this.modalMessages = [{cssClass: 'alert-danger', text: data.message}];
        }
      });
    } else {
      setTimeout(() => {
        this.modalMessages = undefined;
      }, 3000); //Timeout em ms.
    }
  }
}
