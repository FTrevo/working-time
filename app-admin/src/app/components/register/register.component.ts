import { Component, OnInit } from '@angular/core';

import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name: String;
  userName: String;
  email: String;
  password: String;
  compensatoryTime: Boolean;
  previousBalance: Number;

  constructor(
    private validateService: ValidateService,
    private authService: AuthService,
    private flashMessage: FlashMessagesService,
    private router: Router
  ) {}

  ngOnInit() {
    this.compensatoryTime = false;
    this.previousBalance = 0;
  }

  onRegisterSubmit(){
    const user = {
      name: this.name,
      userName: this.userName,
      email: this.email,
      password: this.password,
      compensatoryTime: this.compensatoryTime,
      previousBalance: this.previousBalance
    }

    //Campos obrigatórios
    if(!this.validateService.validateRegister(user)){
      this.flashMessage.show('Preencha todos os campos.', {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      return false;
    }

    //E-mail válido.
    if(!this.validateService.validateEmail(user.email)){
      this.flashMessage.show('E-mail inválido.', {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      return false;
    }

    //Registrar usuário
    this.authService.registerUser(user).subscribe(data => {
      if(data.success){
        this.flashMessage.show(data.message, {cssClass: 'alert-success', timeout: 3000 }); //Timeout em ms.
        this.router.navigate(['/login']);
      } else {
        this.flashMessage.show(data.message, {cssClass: 'alert-danger', timeout: 3000 }); //Timeout em ms.
      }
    });
  }
}
