const Moment = require('moment');

module.exports.getUserFromRequestHeaderAuthorization = (authorization) => {
  let base64Url = authorization.split('.')[1];
  let base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(new Buffer(base64, 'base64').toString('binary'));
}

module.exports.getUserIdFromRequestHeader = (headers) => {
  return this.getUserFromRequestHeaderAuthorization(headers.authorization)._id;
}

module.exports.getUserForInterface = (user) => {
  return {
    _id: user._id,
    name: user.name,
    userName: user.userName,
    email: user.email,
    registrationDate: user.registrationDate,
    compensatoryTime: user.compensatoryTime,
    previousBalance: user.previousBalance
  };
}
