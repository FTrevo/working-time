// --------------- Import de dependências --------------- //
const winston = require('winston');
const Moment = require('moment');

const logger = new (winston.Logger)({
  transports: [ new winston.transports.Console(
      {
        timestamp: function() {
          return Moment().locale('pt-BR').format();
        },
        formatter: function(options) {
          let formattedOutput = '[' + options.timestamp() + '] ['+ winston.config.colorize( options.level, options.level.toUpperCase() )  +'] ';

          if(options.message){
            formattedOutput += '\t Mensagem: ' + options.message;
          }
          if(!(Object.keys(options.meta).length === 0 && options.meta.constructor === Object)){
            formattedOutput += '\t Meta: \t' + JSON.stringify(options.meta, null, '\t');
          }

          return formattedOutput;
        }
      }
    )],
  exitOnError: false
});

// Define o nível do log.
logger.level = 'silly';

module.exports = logger;
