const logger = require('../config/logger.js');

module.exports = (...authorizedRoles) => {
  return function (request, response, next){
    for (authorizedRole of authorizedRoles) {
      if(authorizedRole === request.role){
        next();
        return;
      }
    }
    logger.log('warn', 'Usuário não autorizado tentando realizar uma ação administrativa.');
    logger.log('warn', 'Method: %s ', request.method);
    logger.log('warn', 'OriginalUrl: %s ', request.originalUrl);
    logger.log('warn', 'Usuário: %s ', JSON.stringify(request.user, null, '\t'));
    logger.log('warn', 'RequestBody: %s ', JSON.stringify(request.body, null, '\t'));
    response.json( {success: false, message: 'Não autorizado.'} );
    return;
  }
};
