// --------------- Import de dependências --------------- //
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

// --------------- Import de arquivos do core --------------- //
const User = require('../models/user');
const config = require('../config/database');
const Util = require('./util');

module.exports = (passport) => {
  let options = {};

  options.jwtFromRequest = ExtractJwt.fromAuthHeader();
  options.secretOrKey = config.secret;
  options.passReqToCallback= true;

  passport.use(new JwtStrategy(options, (request, jwt_payload, done) => {
    User.getUserById(jwt_payload._id, (error, user) => {
      if (error) {
        return done(error, false);
      }

      if (user) {
        request.role = user.role;
        return done(null, Util.getUserForInterface(user));
      } else {
        return done(null, false);
      }
    });
  }));
}
