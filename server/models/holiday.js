// --------------- Import de dependências --------------- //
const mongoose = require('mongoose');

// --------------- Import de arquivos do core --------------- //
const config = require('../config/database');

//Schema de feriados.
const HolidaySchema = mongoose.Schema({
  accreditHours: {
    type: Number,
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  description: {
    type: String,
    required: true
  }
});

const Holiday = module.exports = mongoose.model('Holiday', HolidaySchema);

module.exports.getHolidays = (filter, callback) => {
  let query = {};

  if (filter.startDate) {
    query.date = {};
    query.date.$gte = filter.startDate;
  }

  if (filter.endDate) {
    if(!query.date){
      query.date = {};
    }
    query.date.$lte = filter.endDate;
  }

  Holiday.find(query , callback).sort({ 'date': 1 });
}

module.exports.getHolidayById = (id, callback) => {
  Holiday.findById(id, callback);
}

module.exports.getHolidayAtSpecificDate = (date, callback) => {
  let query = { date: date};
  Holiday.findOne(query, callback);
}

module.exports.saveHoliday = (newHoliday, callback) => {
  newHoliday.save(callback);
}

module.exports.updateHoliday = (toBeUpdatedId, toBeUpdatedHoliday, callback) => {
  let query = { _id: toBeUpdatedId};
  Holiday.update(query, toBeUpdatedHoliday, callback);
}

module.exports.removeHolidayById = (id, callback) => {
  let query = { _id: id};
  Holiday.remove(query, callback);
}
