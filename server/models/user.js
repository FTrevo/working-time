// --------------- Import de dependências --------------- //
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

// --------------- Import de arquivos do core --------------- //
const config = require('../config/database');

//Schema do usuário.
const UserSchema = mongoose.Schema({
  name: {
    type: String
  },
  email: {
    type: String,
    required: true
  },
  userName: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  registrationDate: {
    type: Date,
    required: true
  },
  compensatoryTime: {
    type: Boolean,
    requred: true
  },
  previousBalance: {
    type: Number,
    required: true
  },
  role: {
    type: String,
    required: true
  }
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = (id, callback) => {
  User.findById(id, callback);
}

module.exports.getUserByUserName = (userName, callback) => {
  let query = {userName: userName};
  User.findOne(query, callback);
}

module.exports.getUserAdminByUserName = (userName, callback) => {
  let query = {userName: userName, role: 'administrador'};
  User.findOne(query, callback);
}

module.exports.getUserByEmail = (email, callback) => {
  let query = {email: email};
  User.findOne(query, callback);
}

module.exports.addUser = (newUser, callback) => {
  bcrypt.genSalt(10, (error, salt) => {
    bcrypt.hash(newUser.password, salt, (error, hash) => {
      if (error) {
        throw error;
      }
      newUser.password = hash;
      newUser.save(callback);
    });
  });
}

module.exports.comparePassword = (candidatePassword, hash, callback) => {
  bcrypt.compare(candidatePassword, hash, (error, isMatch) => {
    if (error) {
      throw error;
    }
    callback(null, isMatch);
  });
}
