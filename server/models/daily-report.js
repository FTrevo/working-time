// --------------- Import de dependências --------------- //
const mongoose = require('mongoose');

// --------------- Import de arquivos do core --------------- //
const config = require('../config/database');

//Schema do usuário.
const DailyReportSchema = mongoose.Schema({
  userId: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  registrations: {
    type: [{
            start: String,
            end: String
          }]
  },
  dailyResult: {
    type: String,
    required: true
  }
});

const DailyReport = module.exports = mongoose.model('DailyReport', DailyReportSchema);

module.exports.getDailyReportById = (id, callback) => {
  DailyReport.findById(id, callback);
}

module.exports.getDailyReportsByUser = (filter, callback) => {
  let query = {userId: filter.userId};

  if (filter.startDate) {
    query.date = {};
    query.date.$gte = filter.startDate;
  }

  if (filter.endDate) {
    if(!query.date){
      query.date = {};
    }
    query.date.$lte = filter.endDate;
  }

  DailyReport.find(query, callback).sort({ 'date': 1 });
}

module.exports.getDailyReportFromUserAtSpecificDate = (dailyReport, callback) => {
  let query = { userId: dailyReport.userId, date: dailyReport.date};
  DailyReport.findOne(query, callback);
}

module.exports.saveDailyReport = (newDailyReport, callback) => {
  newDailyReport.save(callback);
}

module.exports.updateDailyReport = (toBeUpdatedId, toBeUpdatedDailyReport, callback) => {
  let query = { _id: toBeUpdatedId};
  DailyReport.update(query, toBeUpdatedDailyReport, callback);
}

module.exports.getDailyReportById = (id, callback) => {
  DailyReport.findById(id, callback);
}

module.exports.removeDailyReportById = (id, callback) => {
  let query = { _id: id};
  DailyReport.remove(query, callback);
}
