// --------------- Import de dependências --------------- //
const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');

// --------------- Import de arquivos do core --------------- //
const logger = require('../config/logger.js');
const Util = require('../config/util');
const DailyReport = require('../models/daily-report');

//Rota de consulta de registros diários do usuário
router.get('/list', passport.authenticate('jwt', {session: false}), (request, response, next) => {
  let filter = request.query;
  filter.userId = Util.getUserIdFromRequestHeader(request.headers);

  DailyReport.getDailyReportsByUser(filter, (erroGetDailyReportsByUser, dailyReports) => {
    if(erroGetDailyReportsByUser){
      logger.log('error', erroGetDailyReportsByUser);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    } else {
      response.json( {success: true, dailyReports: dailyReports} );
    }
  });
});

//Rota de cadastro de registro diário
router.post('/save', passport.authenticate('jwt', {session: false}), (request, response, next) => {
  var newDailyReport = new DailyReport({
    userId: Util.getUserIdFromRequestHeader(request.headers),
    date: request.body.date,
    registrations: request.body.registrations,
    dailyResult: request.body.dailyResult
  });

  DailyReport.getDailyReportFromUserAtSpecificDate(newDailyReport, (errorGetDailyReportFromUserAtSpecificDate, savedDailyReport) => {
    if(errorGetDailyReportFromUserAtSpecificDate){
      logger.log('error', errorGetDailyReportFromUserAtSpecificDate);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    }

    if (savedDailyReport) {
      response.json( {success: false, message: 'Já existe um registro diário para a data selecionada.'} );
    } else {
      DailyReport.saveDailyReport(newDailyReport, (errorSaveDailyReport, savedDailyReport) => {
        if(errorSaveDailyReport){
          logger.log('error', errorSaveDailyReport);
          response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
        } else {
          response.json( {success: true, message: 'Registro diário salvo.'} );
        }
      });
    }
  });
});

//Rota de atualização de registro diário
router.post('/update', passport.authenticate('jwt', {session: false}), (request, response, next) => {
  DailyReport.getDailyReportById(request.body._id, (errorGetDailyReportById, dailyReport) => {
    if(errorGetDailyReportById){
      logger.log('error', errorGetDailyReportById);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    }

    if (dailyReport) {
      var updatedDailyReportObject = {
        userId: dailyReport.userId,
        date: dailyReport.date,
        registrations: request.body.registrations,
        dailyResult: request.body.dailyResult,
         __v: (dailyReport.__v + 1)
      };

      DailyReport.updateDailyReport(dailyReport._id, updatedDailyReportObject, (errorUpdateDailyReport, updatedDailyReport) => {
        if(errorUpdateDailyReport){
          logger.log('error', errorUpdateDailyReport);
          response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
        } else {
          response.json( {success: true, message: 'Registro diário atualizado.'} );
        }
      });
    } else {
      response.json( {success: false, message: 'Registro diário não encontrado.'} );
    }
  });
});

//Rota de consulta de registro diário pelo id
router.get('/:id', passport.authenticate('jwt', {session: false}), (request, response, next) => {
  DailyReport.getDailyReportById(request.params.id, (errorGetDailyReportById, dailyReport) => {
    if(errorGetDailyReportById){
      logger.log('error', errorGetDailyReportById);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    }

    if (dailyReport) {
      response.json( {success: true, dailyReport: dailyReport} );
    } else {
      response.json( {success: false, message: 'Registro diário não encontrado.'} );
    }
  });
});

//Rota de remoção de registro diário pelo id
router.delete('/:id', passport.authenticate('jwt', {session: false}), (request, response, next) => {
  DailyReport.removeDailyReportById(request.params.id, (errorRemoveDailyReportById) => {
    if(errorRemoveDailyReportById){
      logger.log('error', errorRemoveDailyReportById);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    } else {
      response.json( {success: true, message: 'Registro diário removido.'} );
    }
  });
});

module.exports = router;
