// --------------- Import de dependências --------------- //
const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');

// --------------- Import de arquivos do core --------------- //
const logger = require('../config/logger.js');
const Holiday = require('../models/holiday');
const AuthorizedRoles = require('../config/role-authorization');

//Rota de consulta de feriados
router.get('/list', passport.authenticate('jwt', {session: false}), (request, response, next) => {
  Holiday.getHolidays(request.query, (errorGetHolidays, holidays) => {
    if(errorGetHolidays){
      logger.log('error', errorGetHolidays);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    } else {
      response.json( {success: true, holidays: holidays} );
    }
  });
});

//Rota de consulta de feriado pelo id
router.get('/:id', passport.authenticate('jwt', {session: false}), (request, response, next) => {
  Holiday.getHolidayById(request.params.id, (errorGetHolidayById, holiday) => {
    if(errorGetHolidayById){
      logger.log('error', errorGetHolidayById);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    }

    if (holiday) {
      response.json( {success: true, holiday: holiday} );
    } else {
      response.json( {success: false, message: 'Feriado não encontrado.'} );
    }
  });
});

//Rota de cadastro de feriado
router.post('/save', passport.authenticate('jwt', {session: false}), AuthorizedRoles('administrador'), (request, response, next) => {
  var newHoliday = new Holiday({
    date: request.body.date.jsdate,
    accreditHours: request.body.accreditHours,
    description: request.body.description
  });

  Holiday.getHolidayAtSpecificDate(newHoliday.date, (errorGetHolidayAtSpecificDate, savedHoliday) => {
    if(errorGetHolidayAtSpecificDate){
      logger.log('error', errorGetHolidayAtSpecificDate);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    }

    if (savedHoliday) {
      response.json( {success: false, message: 'Já existe um feriado para a data selecionada.'} );
    } else {
      Holiday.saveHoliday(newHoliday, (errorSaveHoliday, savedHoliday) => {
        if(errorSaveHoliday){
          logger.log('error', errorSaveHoliday);
          response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
        } else {
          response.json( {success: true, message: 'Feriado salvo.'} );
        }
      });
    }
  });
});

//Rota de atualização de feriado
router.post('/update', passport.authenticate('jwt', {session: false}), (request, response, next) => {
  Holiday.getHolidayById(request.body._id, (errorGetHolidayById, holiday) => {
    if(errorGetHolidayById){
      logger.log('error', errorGetHolidayById);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    }

    if (holiday) {
      var updatedHolidayObject = {
        date: holiday.date,
        accreditHours: request.body.accreditHours,
        description: request.body.description,
         __v: (holiday.__v + 1)
      };

      Holiday.updateHoliday(holiday._id, updatedHolidayObject, (errorUpdateHoliday, updatedHoliday) => {
        if(errorUpdateHoliday){
          logger.log('error', errorUpdateHoliday);
          response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
        } else {
          response.json( {success: true, message: 'Feriado atualizado.'} );
        }
      });
    } else {
      response.json( {success: false, message: 'Feriado não encontrado.'} );
    }
  });
});

//Rota de remoção de feriado pelo id
router.delete('/:id', passport.authenticate('jwt', {session: false}), (request, response, next) => {
  Holiday.removeHolidayById(request.params.id, (errorRemoveHolidayById) => {
    if(errorRemoveHolidayById){
      logger.log('error', errorRemoveHolidayById);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    } else {
      response.json( {success: true, message: 'Feriado removido.'} );
    }
  });
});

module.exports = router;
