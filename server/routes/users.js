// --------------- Import de dependências --------------- //
const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const Moment = require('moment');

// --------------- Import de arquivos do core --------------- //
const config = require('../config/database');
const logger = require('../config/logger.js');
const User = require('../models/user');
const Util = require('../config/util');

//Rota de registro de usuário
router.post('/register', (request, response, next) => {
  let balance = 0;
  if(request.body.compensatoryTime && request.body.previousBalance){
    balance = request.body.previousBalance;
  }

  var newUser = new User({
    name: request.body.name,
    email: request.body.email,
    userName: request.body.userName,
    password: request.body.password,
    registrationDate: Moment().startOf('day'),
    compensatoryTime: request.body.compensatoryTime,
    previousBalance: balance,
    role: 'usuario'
  });

  User.getUserByUserName(newUser.userName, (errorGetByUserName, userUserName) => {
    if(errorGetByUserName){
      logger.log('error', errorGetByUserName);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    }

    if (userUserName) {
      response.json( {success: false, message: 'Usuário já existente.'} );
    } else {

      User.getUserByEmail(newUser.email, (errorGetByEmail, userEmail) => {
        if(errorGetByEmail){
          logger.log('error', errorGetByEmail);
          response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
        }

        if (userEmail) {
          response.json( {success: false, message: 'Este e-mail já está cadastrado.'} );
        } else {

          User.addUser(newUser, (errorAddUser, user) => {
            if(errorAddUser){
              logger.log('error', errorAddUser);
              response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
            } else {
              response.json( {success: true, message: 'Usuário registrado.'} );
            }
          });
        }
      });
    }
  });
});

//Rota de autenticação de usuário
router.post('/authenticate', (request, response, next) => {
  const userName = request.body.userName;
  const password = request.body.password;

  User.getUserByUserName(userName, (errorGetByUserName, user) => {
    if(errorGetByUserName){
      logger.log('error', errorGetByUserName);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    }

    if (!user) {
      response.json( {success: false, message: 'Usuário não encontrado'} );
    } else {

      User.comparePassword(password, user.password, (authenticationError, isMatch) => {
        if(authenticationError){
          logger.log('error', authenticationError);
          response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
        }

        if (isMatch) {
          let interfaceUser = Util.getUserForInterface(user);
          const token = jwt.sign(interfaceUser, config.secret, { expiresIn: 21600 } ); //Tempo em segundos (6h)
          response.json({
            success: true,
            token: 'JWT ' + token,
            user: interfaceUser
          });
        } else {
          response.json( {success: false, message: 'Senha inválida.'} );
        }
      });
    }
  });
});

//Rota de autenticação de admin
router.post('/authenticate-admin', (request, response, next) => {
  const userName = request.body.userName;
  const password = request.body.password;

  User.getUserAdminByUserName(userName, (errorGetByUserName, user) => {
    if(errorGetByUserName){
      logger.log('error', errorGetByUserName);
      response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
    }

    if (!user) {
      response.json( {success: false, message: 'Administrador não encontrado'} );
    } else {

      User.comparePassword(password, user.password, (authenticationError, isMatch) => {
        if(authenticationError){
          logger.log('error', authenticationError);
          response.json( {success: false, message: 'Ocorreu um erro inesperado no sistema, favor tente novamente mais tarde.'} );
        }

        if (isMatch) {
          let interfaceUser = Util.getUserForInterface(user);
          const token = jwt.sign(interfaceUser, config.secret, { expiresIn: 21600 } ); //Tempo em segundos (6h)
          response.json({
            success: true,
            token: 'JWT ' + token
          });
        } else {
          response.json( {success: false, message: 'Senha inválida.'} );
        }
      });
    }
  });
});

//Rota de perfil
router.get('/profile', passport.authenticate('jwt', {session: false}), (request, response, next) => {
  response.json( {user: request.user} );
  //FIXME - Será implementado mais tarde.
});

module.exports = router;
