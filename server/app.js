// --------------- Import de dependências --------------- //
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
// --------------- Import de arquivos do core --------------- //
const config = require('./config/database');
const logger = require('./config/logger.js');

// --------------- Conexão com o banco --------------- //
mongoose.Promise = global.Promise;
mongoose.connect(config.database);

mongoose.connection.on('connected', () => {
  logger.log('info', 'Conectado ao banco de dados ' + config.database);
});

mongoose.connection.on('error', (errorConnectingToDatabase) => {
  logger.log('error', 'Erro ao conectar ao banco de dados ' + errorConnectingToDatabase);
});

// --------------- Configuração do app --------------- //
const app = express();
const port = 3000;
// const port = process.env.PORT || 8080;
app.use(express.static(path.join(__dirname, 'public')));

// -------------------- Middlewares -------------------- //
//Cors Middleware
app.use(cors());
//BodyParser Middleware
app.use(bodyParser.json());
//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

// --------------- Definição de rotas --------------- //
const users = require('./routes/users');
app.use('/users', users);
const dailyReports = require('./routes/daily-reports');
app.use('/daily-reports', dailyReports);
const holidays = require('./routes/holidays');
app.use('/holidays', holidays);

app.get('*', (request, response) => {
  response.sendFile(path.join(__dirname, 'index.html'));
});

// --------------- Inicialização do app --------------- //
const server = app.listen(port, () => {
  logger.log('info', 'Servidor inicializado.');
  logger.log('info', 'Host: ' + server.address().address);
  logger.log('info', 'Porta: ' + server.address().port);
}).on('error', (serverError) => {
  logger.log('error', serverError);
});
